package corso.mongo.MongoSimple;

import java.net.UnknownHostException;
import java.util.ArrayList;

import org.bson.Document;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import static com.mongodb.client.model.Filters.*;

import corso.mongo.MongoSimple.models.Libro;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) 
    {

    	MongoClientURI connectionUri = new MongoClientURI("mongodb://localhost:27017");
    	
    	try {
			
			/*
			 * {
			 * 		_id: "123456",
			 * 		"titolo": "La tempesta",
			 * 		"autore": "W.S.",
			 * 		"casa_editrice": {
			 * 			"via": "Via degli equinozi",
			 * 			"citta": "Milano"
			 * 		},
			 * 		"categorie": [ "Opera teatrale", "Classico letterario" ],			//cat
			 * 		"ristampe": [														//ris
			 * 			{
			 * 				"anno": 2010,
			 * 				"formato": "Copertina flessibile"
			 * 			},
			 * 			{
			 * 				"anno": 2012,
			 * 				"formato": "Copertina morbida"	
			 * 			}
			 * 		]
			 * }
			 */
			
//			MongoClient client = new MongoClient(connectionUri);
//			
//			DB database = (DB) client.getDatabase("Libreria");
//			DBCollection coll_libri = database.getCollection("archivio");
    		
			//INSERIMENTO DI UN OGGETTO
//			BasicDBList cat = new BasicDBList();
//			cat.add("Opera teatrale");
//			cat.add("Classico letterario");
//			
//			BasicDBList ris = new BasicDBList();
//			ris.add( new BasicDBObject("anno", 2010).append("formato", "Copertina flessibile") );
//			ris.add( new BasicDBObject("anno", 2012).append("formato", "Copertina morbida") );
//		
//			DBObject libro = new BasicDBObject("_id", "123456")
//					.append("titolo", "La tempesta")
//					.append("autore", "W.S.")
//					.append("casa_editrice", 
//								new BasicDBObject("via", "Via degli equinozi").append("citta", "Milano")
//							)
//					.append("categorie", cat)
//					.append("ristampe", ris);
//			
//			
//			coll_libri.insert(libro);
//			
//			System.out.println("Operazione effettuata con successo");
			
			//SCANSIONE
//			DBCursor cursor = coll_libri.find( new BasicDBObject("autore", "W.S.") );
//			
//			ArrayList<Libro> elenco = new ArrayList<Libro>();
//			
//			for(DBObject temp: cursor) {
//				System.out.println(temp.get("titolo"));
//				
//				Libro libroTemp = new Libro();
//				
//				libroTemp.setAutore((String) temp.get("autore"));
//				libroTemp.setTitolo((String) temp.get("titolo"));
//				
//				elenco.add(libroTemp);
//			}
			
			//TODO: Remove
			
			//TODO: Update
			
    		
    		//////////////////////////////////////////////// Utilizzo del Document
    		
    		MongoClient client = new MongoClient(connectionUri);
    		
    		MongoDatabase database = client.getDatabase("Libreria");
    		MongoCollection<Document> coll = database.getCollection("archivio");
    		
//    		Document doc = new Document("_id", "789452")
//    				.append("titolo", "Othello")
//					.append("autore", "W.S.");
//    		
//    		coll.insertOne(doc);
    		
    		//Ricerca semplice di unico oggetto
//    		Document libroTemp = coll.find().first();
//    		System.out.println(libroTemp.toJson());
    		
//    		Document libroTemp = coll.find(Filters.eq("titolo", "PROVA")).first();
//    		System.out.println(libroTemp.toJson());
			
    		//Visualizazione di documenti multipli
//    		MongoCursor<Document> elenco = coll.find().iterator();
//    		
//    		while(elenco.hasNext()) {
//    			System.out.println(elenco.next().toJson());
//    		}
    		
    		coll.updateOne(eq("titolo", "La tempesta"), new Document("$set", new Document("autore", "Giovanni")));
    		
    		//UPDATE MANY
    		
    		//DELETE
    		
    		
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
    	
    	
    	
    }
}
